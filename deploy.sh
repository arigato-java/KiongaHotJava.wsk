#!/bin/bash -e
BX_CMD=${HOME}/Bluemix_CLI/bin/bluemix
if [ -f action.zip ]; then
	unlink action.zip
fi

pushd src
npm install
zip -9r ../action.zip package.json hotjava.js node_modules
popd

$BX_CMD cloud-functions action create hotjava2 --kind nodejs:8 action.zip --memory 128 --timeout 10000 --web false --param-file secrets.json

