'use strict';
const DarkSky=require('dark-sky');
const Twitter=require('twitter');

function KiongaHotJava(options){
	// options { darksky: { key: string darksky api key, query: obj darksky query}, twitter: obj twitter credentials}
	this.options=options;
	this.darksky=new DarkSky(this.options.darksky.key);
}
KiongaHotJava.prototype.run=function(){
	return this.darksky.options(this.options.darksky.query).get()
		.then(this.gotWeather.bind(this));
};
KiongaHotJava.prototype.gotWeather=function(response){
	var data0=response.daily.data[0];
	var today_max=Number(data0.temperatureHigh);
	var hotjava_time=new Date(1000*data0.temperatureHighTime).toLocaleString("ja-JP",{timeZone: "Asia/Tokyo"});
	var java_msg=this.selectJavaMessage(today_max);
	if(!java_msg){
		return;
	}
	var post_message=[java_msg,hotjava_time,data0.summary].join(' ');
	return this.postResult(post_message);
};
KiongaHotJava.prototype.selectJavaMessage=function(forTemperatureCelsius){
	const java_tbl=[
		[28,'今日の気温がHotJava'],
		[35,'今日の気温はモットジャバ'],
		[37,'今日も気温がモットジャバ']];
	var res;
	java_tbl.forEach((tpl)=>{res=(tpl[0]<=forTemperatureCelsius)?tpl[1]:res;});
	return res;
};
KiongaHotJava.prototype.postResult=function(msg){
	var client=new Twitter(this.options.twitter);
	return client.post('statuses/update',{'status':msg});
};
function main(params){
	var khj=new KiongaHotJava({
	'darksky':{
		'key':params.DARKSKY_API_KEY,
		'query':{
			'latitude': 34.7649,
			'longitude': 135.5174,
			'language': 'ja',
			'exclude': 'currently,minutely,hourly,alerts,flags',
			'units': 'si'
		}
	},
	'twitter':{
		'consumer_key':params.TWITTER_CONSUMER_KEY,
		'consumer_secret':params.TWITTER_CONSUMER_SECRET,
		'access_token_key':params.TWITTER_ACCESS_TOKEN_KEY,
		'access_token_secret':params.TWITTER_ACCESS_TOKEN_SECRET
	}
	});
	return khj.run().catch(console.log);
}
exports.main=main;

// debug
/*
(async function(){await main({
	'DARKSKY_API_KEY': process.env.DARKSKY_API_KEY,	
	'TWITTER_CONSUMER_KEY': process.env.TWITTER_CONSUMER_KEY,
	'TWITTER_CONSUMER_SECRET': process.env.TWITTER_CONSUMER_SECRET,
	'TWITTER_ACCESS_TOKEN_KEY': process.env.TWITTER_ACCESS_TOKEN_KEY,
	'TWITTER_ACCESS_TOKEN_SECRET': process.env.TWITTER_ACCESS_TOKEN_SECRET
});})();
*/
